package com.geektechnique.tracing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class MockController {

    @Autowired TracingApplication tracingApplication;

    public MockController(TracingApplication tracingApplication){
        this.tracingApplication = tracingApplication;
    }

    private final static Logger LOG = Logger.getLogger(TracingApplication.class.getName());

    @RequestMapping("/")
    public String index(){
        LOG.log(Level.INFO, "call from index api");
        return "welcome sleuth";
    }
}
